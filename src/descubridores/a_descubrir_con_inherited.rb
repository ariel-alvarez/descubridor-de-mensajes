class ADescubrirConInherited
  def self.inherited(child_class)

    # Creamos el atributo de clase para la sublcase recién generada
    child_class.instance_variable_set(:@mensajes_recibidos, [])

    # Definimos el método de clase que hace de accessor para el atributo
    child_class.define_singleton_method(:mensajes_que_recibi) do
      @mensajes_recibidos
    end

    child_class.define_singleton_method(:cuantos_mensajes_recibi) do
      @mensajes_recibidos.count
    end

    child_class.define_singleton_method(:resetear_contador_de_mensajes_recibidos) do
      @mensajes_recibidos = []
    end

    child_class.define_singleton_method(:agregar_metodo) do |nameSymbol|
      unless @mensajes_recibidos.include? nameSymbol
        @mensajes_recibidos.push(nameSymbol)
      end
    end

    # Definimos un método de instancia que sobreescribe method missing
    # para todas las instancias de la sublcase recién creada
    child_class.send(:define_method, :__on_method_not_found) do |name|
      child_class.agregar_metodo(name)
    end

    super
  end

  def __on_method_not_found(name)
    throw 'La clase descubridora sólo sirve para ser extendida, no le hables!'
  end

  def method_missing(name, *args)
    self.__on_method_not_found(name)
  end
end