class ADescubrirConFlechitas
  class << self
    attr_accessor :metodos_requeridos
    attr_accessor :subclases
  end

  def self.metodos_requeridos
    @metodos_requeridos ||= Set.new([])
  end

  def self.subclases
    @subclases ||= Set.new([])
  end

  def self.inherited(child)
    self.subclases.add(child)
  end

  def method_missing(name, *args)
      self.class.metodos_requeridos.add(name)
  end

  def self.mensajes_recibidos_totales
    self.metodos_requeridos.merge(self.subclases.map{ |subclase| subclase.mensajes_recibidos_totales }).flatten
  end

end