require '../src/descubridores/a_descubrir_con_flechitas'
require 'rspec'

describe 'Cada subclase de ADescubrir' do

  class Animal < ADescubrirConFlechitas
  end

  class Perro < Animal
  end

  class Gato < Animal
  end

  before(:each) do
    # Reseteamos los acumuladores de mensajes recibidos
    Perro.metodos_requeridos  = Set.new([])
    Gato.metodos_requeridos   = Set.new([])
    Animal.metodos_requeridos = Set.new([])
  end

  it 'debe tener una lista con los mensajes que se le enviaron y no entendió' do
    Perro.new.ladra
    Perro.new.juega
    expect(Perro.metodos_requeridos).to eq(Set.new([:ladra, :juega]))

  end

  it 'debe tener una lista con los mensajes que se le enviaron y no entendió, sin repetidos' do
    Perro.new.ladra
    Perro.new.ladra
    expect(Perro.metodos_requeridos).to eq(Set.new([:ladra]))
  end

  it 'debe tener una lista con los mensajes que se le enviaron y no entendió, independiente para cada sublcase' do
    Perro.new.ladra
    Gato.new.maulla

    expect(Perro.metodos_requeridos).to eq(Set.new([:ladra]))
    expect(Gato.metodos_requeridos).to eq(Set.new([:maulla]))
  end

  it 'si tiene subclases, sus mensajes_recibidos_totales incluyen aquellos recibidos por las subclases' do
    Perro.new.ladra
    Gato.new.maulla
    Animal.new.come

    expect(Perro.mensajes_recibidos_totales).to eq(Set.new [:ladra])
    expect(Gato.mensajes_recibidos_totales).to eq(Set.new [:maulla])
    expect(Animal.mensajes_recibidos_totales).to eq(Set.new [:ladra, :maulla, :come])
  end


end

