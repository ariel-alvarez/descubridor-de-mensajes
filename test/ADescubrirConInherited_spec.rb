require '../src/descubridores/a_descubrir_con_inherited'
require 'rspec'

describe 'Cada subclase de ADescubrir' do

  class Animal < ADescubrirConInherited
  end

  class Perro < Animal
  end

  class Gato < Animal
  end

  before(:each) do
    Perro.resetear_contador_de_mensajes_recibidos
    Gato.resetear_contador_de_mensajes_recibidos
    Animal.resetear_contador_de_mensajes_recibidos
  end

  it 'debe tener una lista con los mensajes que se le enviaron y no entendió' do
    Perro.new.ladra
    Perro.new.juega
    expect(Perro.mensajes_que_recibi).to eq([:ladra, :juega])

  end

  it 'debe tener una lista con los mensajes que se le enviaron y no entendió' do
    Perro.new.ladra
    Gato.new.maulla
    Animal.new.come

    expect(Animal.mensajes_que_recibi).to eq([:come])
    expect(Perro.mensajes_que_recibi).to eq([:ladra])
    expect(Gato.mensajes_que_recibi).to eq([:maulla])

  end


end

